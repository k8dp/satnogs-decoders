#!/usr/bin/env python3
import re
import argparse
import xml.etree.ElementTree as ET

TAB_SIZE = 2

class KaitaiValueDocElement():
    def __init__(self, label, value):
        self._label = label
        self._value = value

    def create_kaitai_value_doc_element(self, file):
        file.write("\t\t\t\t\t".expandtabs(TAB_SIZE) + self._label + ": " + self._value + "\n")

class KaitaiDoc():
    def __init__(self):
        self._elements = []

    def add_doc_element(self, doc_element):
        self._elements.append(doc_element)

    def create_kaitai_doc(self, file):
        file.write("\t\t\t\tdoc: |\n".expandtabs(TAB_SIZE))
        for element in self._elements:
            element.create_kaitai_value_doc_element(file)

class KaitaiType():
    def __init__(self, type_string, doc=None):
        self._type_string = type_string
        self._doc = doc

    def get_type_string(self):
        return self._type_string
    
    def create_kaitai_type(self, file):
        file.write("\t\t\t\ttype: ".expandtabs(TAB_SIZE) + self._type_string + "\n")
        if self._doc is not None:
            self._doc.create_kaitai_doc(file)

class KaitaiProperty():
    def __init__(self, name, type=None):
        self._name = name
        self._type = type

    def add_type(self, type):
        self._type = type

    def get_name(self):
        return self._name
    
    def get_type(self):
        return self._type
    
    def create_kaitai_property(self, file):
        file.write("\t\t\t- id: ".expandtabs(TAB_SIZE) + self._name + "\n")
        self._type.create_kaitai_type(file)
    
class KaitaiSeq():
    def __init__(self, name, is_root=False):
        self._name = name
        self._properties = []
        self._is_root = is_root

    def add_property(self, property):
        self._properties.append(property)

    def get_properties(self):
        return self._properties
    
    def get_name(self):
        return self._name
    
    def set_is_root(self, is_root):
        self._is_root = is_root
    
    def create_kaitai_seq(self, file):
        if self._is_root:
            file.write("\nseq:\n\t- id: ".expandtabs(TAB_SIZE) + self._name + "\n\t\ttype: ".expandtabs(TAB_SIZE) + self._name + "_type\n\ntypes:\n\t".expandtabs(TAB_SIZE) + self._name + "_type")
        else:
            file.write("\n\t".expandtabs(TAB_SIZE) + self._name)

        file.write("\n\t\tseq:\n".expandtabs(TAB_SIZE))
        for property in self._properties:
            property.create_kaitai_property(file)


def get_namespace(element):
    m = re.match(r'\{.*\}', element.tag)
    return m.group(0) if m else ''


def get_tag_without_namespace(tag):
    m = re.search(r'(?<=\})(.*)', tag)
    return m.group(0) if m else ''


def get_size_in_bits(element):
    encoding = element.find(".//*[@sizeInBits]")
    return encoding.attrib['sizeInBits']


def is_signed(element):
    return element.attrib['signed']

def get_size_in_bytes(size_in_bits):
    if size_in_bits % 8 == 0:
        return int(size_in_bits / 8)

    return None

def get_kaitai_type(size, signed=None):
    size_in_bytes = get_size_in_bytes(size)
    if size_in_bytes is None:
        return "b" + str(size)
    
    if signed is None or signed is False:
        return "u" + str(size_in_bytes)
    
    return "s" + str(size_in_bytes)


def create_ksy_header(sat_name, endianess, file):
    file.write("---\nmeta:\n\tid: ".expandtabs(TAB_SIZE) + sat_name + "\n\tendian: ".expandtabs(TAB_SIZE) + endianess + "\n")


def get_kaitai_type_from_xtce(element):
    tag = get_tag_without_namespace(element.tag)
    match tag:
        case "FloatParameterType":
            size = get_size_in_bits(element)
            return KaitaiType("f" + str(get_size_in_bytes(int(size))))
        case "BooleanParameterType":
            size = get_size_in_bits(element)
            kaitai_doc = None
            if "zeroStringValue" in element.attrib:
                kaitai_doc = KaitaiDoc()
                kaitai_doc.add_doc_element(KaitaiValueDocElement("0", element.attrib['zeroStringValue']))
                kaitai_doc.add_doc_element(KaitaiValueDocElement("1", element.attrib['oneStringValue']))
            
            return KaitaiType(get_kaitai_type(int(size)), doc=kaitai_doc)
        case "IntegerParameterType":
            size = get_size_in_bits(element)
            signed = is_signed(element)
            return KaitaiType(get_kaitai_type(int(size), signed))
        case "EnumeratedParameterType":
            docs = element.findall(".//*[@label]")
            kaitai_doc = KaitaiDoc()
            for doc in docs:
                kaitai_doc.add_doc_element(KaitaiValueDocElement(doc.attrib['label'], doc.attrib['value']))

            size = get_size_in_bits(element)
            return KaitaiType(get_kaitai_type(int(size)), doc=kaitai_doc)
        case "BinaryParameterType":
            namespace = get_namespace(element)
            size = element.find('{0}BinaryDataEncoding/{0}SizeInBits/{0}FixedValue'.format(namespace)).text
            return KaitaiType(get_kaitai_type(int(size)))
        case "AggregateParameterType":
            return None


def create_aggregate_parameter_type(aggregate_param_type, root, file):
    kaitai_seq = KaitaiSeq(aggregate_param_type.attrib['name'])
    members = aggregate_param_type.findall(".//*[@name]")
    for member in members:
        kaitai_property = KaitaiProperty(member.attrib['name'])
        type = root.find(".//*[@name='{0}']".format(member.attrib['typeRef']))
        kaitai_type = get_kaitai_type_from_xtce(type)
        if kaitai_type is None:
            kaitai_property.add_type(KaitaiType(member.attrib['typeRef']))

        else:
            kaitai_property.add_type(kaitai_type)
            
        kaitai_seq.add_property(kaitai_property)

    kaitai_seq.create_kaitai_seq(file)


def create_aggregate_parameter_types(root, namespace, file):
    aggregate_param_types = root.findall(".//*{0}AggregateParameterType".format(namespace))
    for type in aggregate_param_types:
        create_aggregate_parameter_type(type, root, file)


def create_container_types(root, namespace, file):
    root_exists = False
    kaitai_seqs = []
    for container in root.iter('{0}SequenceContainer'.format(namespace)):
        kaitai_seq = KaitaiSeq(container.attrib['name'])
        for parameter_set in container.findall('{0}EntryList/{0}ParameterRefEntry'.format(namespace)):
            parameter = root.find(".//*{0}ParameterSet/{0}Parameter[@name='{1}']".format(namespace, parameter_set.attrib['parameterRef']))
            kaitai_property = KaitaiProperty(parameter.attrib['name'])
            type = root.find(".//*[@name='{0}']".format(parameter.attrib['parameterTypeRef']))
            kaitai_type = get_kaitai_type_from_xtce(type)
            if kaitai_type is None:
                kaitai_property.add_type(KaitaiType(type.attrib['name']))
            else:
                kaitai_property.add_type(kaitai_type)
            
            kaitai_seq.add_property(kaitai_property)

        if root_exists is False and "abstract" in container.attrib:
            kaitai_seq.set_is_root(bool(container.attrib['abstract']))
            root_exists = True
            kaitai_seqs.insert(0, kaitai_seq)
        else:
            kaitai_seqs.append(kaitai_seq)

    for seq in kaitai_seqs:
        seq.create_kaitai_seq(file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate the kaitai structure from '
                                     'a given xtce file. '
                                     'NOTE: This creates a template only, which has to be manually finished!')
    parser.add_argument('xtce_file',
                        type=str,
                        help='Xtce file')
    parser.add_argument('sat_name',
                        type=str,
                        help='Satellite name')
    parser.add_argument('--endianess', type=str,
                            help='The endianess to apply (Default is be)', default='be')
    args = parser.parse_args()

filename = args.sat_name + ".ksy"
f = open(filename, "w")
create_ksy_header(args.sat_name, args.endianess, f)

tree = ET.parse(args.xtce_file)
root = tree.getroot()
namespace = get_namespace(root)

create_container_types(root, namespace, f)
create_aggregate_parameter_types(root, namespace, f)

f.close()
print("Template file {0} generated!".format(filename))
